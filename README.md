# petstore-testing

# Petstore Tests
Проект представляет собой автотесты для сервиса Petstore. Тесты написаны на Java с использованием TestNG и RestAssured.

Все создаваемые записи в сервисе удаляются после прогона тестов.

# Требования
- Java 11
- Gradle
- Docker
- Docker Compose

# Структура проекта
- src/main/java/org/petstore/models: используемые модели данных
- src/test: автотесты
  - controllers: валидационные тесты контроллеров
  - usecases: пользовательские сценарии
- src/test/resources: ресурсы для тестов
- docker-compose.yml: файл конфигурации Docker Compose

# Запуск тестов

```bash
docker-compose build
````

```bash
docker-compose up
```

## Генерация отчетов

Для генерации отчетов используется Allure Framework. После выполнения тестов, выполните команду:

```bash
./gradlew allureReport
```

Для просмотра отчётов после их генерации, выполните команду:

```bash
./gradlew allureServe
```