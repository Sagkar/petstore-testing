package org.petstore.models;

import java.util.Date;

public class Order {
    long id;

    long petId;

    long quantity;

    Date shipDate;

    String status;

    boolean complete;

    public long getId() {
        return id;
    }

    public long getPetId() {
        return petId;
    }

    public long getQuantity() {
        return quantity;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public String getStatus() {
        return status;
    }

    public boolean isComplete() {
        return complete;
    }
}
