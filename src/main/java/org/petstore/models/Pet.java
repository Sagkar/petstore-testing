package org.petstore.models;

import java.util.Map;

public class Pet {
    long id;

    String name;

    Map<String, String> protoUrls;

    String status;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getProtoUrls() {
        return protoUrls;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", protoUrls=" + protoUrls +
                ", status='" + status + '\'' +
                '}';
    }
}
