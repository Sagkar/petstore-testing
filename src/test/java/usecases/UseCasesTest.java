package usecases;

import com.google.gson.Gson;

import controllers.PetControllerTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

import org.petstore.models.Order;
import org.petstore.models.Pet;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

import static org.testng.Assert.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UseCasesTest {

    private static final Logger logger = LoggerFactory.getLogger(PetControllerTest.class);

    private long orderId;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @AfterClass
    public void tearDown() {

        //logging out
        RestAssured.given()
                .when()
                .get("/user/logout")
                .then()
                .extract()
                .response();

        //checking that user exist
        Response response = RestAssured.given()
                .when()
                .get("/user/username")
                .then()
                .extract()
                .response();

        //deleting user if exist
        if (response.getStatusCode() == 200) {
            RestAssured.given()
                    .contentType(ContentType.JSON)
                    .when()
                    .delete("/user/username")
                    .then()
                    .extract()
                    .response();
        }

        //deleting the pet
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/pet/3456")
                .then()
                .extract()
                .response();

        //deleting order
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/order/{orderId}", orderId)
                .then()
                .extract()
                .response();
    }

    @Test
    public void testCreateUserAndChangePassword() {
        String reqBody = "{\n" +
                "  \"id\": 3456,\n" +
                "  \"username\": \"username\",\n" +
                "  \"firstName\": \"First\",\n" +
                "  \"lastName\": \"Last\",\n" +
                "  \"email\": \"string@test.com\",\n" +
                "  \"password\": \"pass1231pass\",\n" +
                "  \"phone\": \"555-5555\",\n" +
                "  \"userStatus\": 0\n" +
                "}";

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(reqBody)
                .when()
                .post("/user")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Create user successful");
        } else {
            logger.error("Error creating user, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);

        response = RestAssured.given()
                .queryParam("username", "username")
                .queryParam("password", "pass1231pass")
                .when()
                .get("/user/login")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Login successful");
        } else {
            logger.error("Login error, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);

        String updateRequest = "{\n" +
                "  \"password\": \"qwerty19\"\n" +
                "}";
        response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(updateRequest)
                .when()
                .put("/user/username")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Password update successful");
        } else {
            logger.error("Error updating user, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void testAddPetAndAddPhoto() {
        String addPetReq = "{\n" +
                "  \"id\": 1234,\n" +
                "  \"name\": \"homelander\",\n" +
                "  \"status\": \"pending\"\n" +
                "}";

        File petPhoto = new File("src/test/resources/home.jpg");

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(addPetReq)
                .when()
                .post("/pet")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Adding new pet successful");
        } else {
            logger.error("Error adding pet, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);

        response = given()
                .formParam("test", "test")
                .multiPart(petPhoto)
                .when().post("/pet/1234/uploadImage");

        if (response.getStatusCode() == 200) {
            logger.info("Photo added successfully");
        } else {
            logger.error("Error adding photo, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void testFindAvailablePetAndOrder() {
        Response response = RestAssured.given()
                .queryParam("status", "available")
                .when()
                .get("/pet/findByStatus")
                .then()
                .extract()
                .response();

        Gson g = new Gson();
        Pet[] pets = g.fromJson(response.asString(), Pet[].class);
        Pet pet = pets[0];

        if (response.getStatusCode() == 200) {
            logger.info("Got the list of available pet successfully");
        } else {
            logger.error("Error getting the list of pets, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);

        String reqBody = g.toJson(pet);

        response = given()
                .contentType("application/json")
                .body(reqBody)
                .post("/store/order");

        if (response.getStatusCode() == 200) {
            logger.info("Pet ordered successfully");
        } else {
            logger.error("Error ordering the pet, service response: \n" + response.asString());
        }

        Order order = g.fromJson(response.asString(), Order.class);
        assertEquals(response.getStatusCode(), 200);
        orderId = order.getId();

        response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get("store/order/{orderId}", orderId)
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Purchase order found successfully");
        } else {
            logger.error("Error getting purchase order, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }
}
