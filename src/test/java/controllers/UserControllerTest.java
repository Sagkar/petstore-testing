package controllers;

import com.google.gson.Gson;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.petstore.models.User;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import static org.testng.Assert.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(PetControllerTest.class);

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2/user";
    }

    @AfterClass
    public void tearDown() {
        Response response = RestAssured.given()
                .when()
                .get("/UpdatedUsername")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            RestAssured.given()
                    .contentType(ContentType.JSON)
                    .when()
                    .delete("/UpdatedUsername")
                    .then()
                    .extract()
                    .response();
        }
    }

    @Test(priority = 1, groups = "users")
    public void testCreateUser() {
        String reqBody = "{\n" +
                "  \"id\": 3456,\n" +
                "  \"username\": \"username\",\n" +
                "  \"firstName\": \"string\",\n" +
                "  \"lastName\": \"string\",\n" +
                "  \"email\": \"string\",\n" +
                "  \"password\": \"password123\",\n" +
                "  \"phone\": \"string\",\n" +
                "  \"userStatus\": 0\n" +
                "}";

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(reqBody)
                .when()
                .post()
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("User created successfully");
        } else {
            logger.error("Error creating the user, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 2, dependsOnMethods = "testCreateUser", groups = "users")
    public void testLogin() {
        Response response = RestAssured.given()
                .queryParam("username", "username")
                .queryParam("password", "password123")
                .when()
                .get("/login")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Login successful");
        } else {
            logger.error("Login error, service response: " + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 3, dependsOnMethods = "testLogin", groups = "users")
    public void testUpdateUser() {
        String reqBody = "{\n" +
                "  \"username\": \"UpdatedUsername\",\n" +
                "  \"firstName\": \"First\",\n" +
                "  \"lastName\": \"Last\",\n" +
                "  \"phone\": \"555-555\",\n" +
                "  \"userStatus\": 1\n" +
                "}";
        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(reqBody)
                .when()
                .put("/username")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("User updated successfully");
        } else {
            logger.error("Error updating the user, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 4, dependsOnMethods = "testUpdateUser", groups = "users")
    public void testGetUser() {
        Response response = RestAssured.given()
                .when()
                .get("/UpdatedUsername")
                .then()
                .extract()
                .response();

        Gson g = new Gson();
        User user = g.fromJson(response.asString(), User.class);

        if (response.getStatusCode() == 200) {
            logger.info("User received successfully");
        } else {
            logger.error("Error receiving the user, service response: \n" + response.asString());
        }

        assertEquals(user.getUsername(), "UpdatedUsername");
        assertEquals(user.getPhone(), "555-555");
        assertEquals(user.getUserStatus(), 1);
        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 5, dependsOnMethods = "testGetUser", groups = "users")
    public void testLogout() {
        Response response = RestAssured.given()
                .when()
                .get("/logout")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Logout successful");
        } else {
            logger.error("Logout error, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 6, dependsOnMethods = "testLogout", groups = "users")
    public void testDeleteUser() {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/UpdatedUsername")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("User deleted successfully");
        } else {
            logger.error("Error deleting user, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }
}
