package controllers;

import com.google.gson.Gson;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.petstore.models.Pet;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import static org.testng.Assert.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PetControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(PetControllerTest.class);

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @AfterClass
    public void tearDown() {
        RestAssured
                .given()
                .when()
                .delete(RestAssured.baseURI + "/pet/" + 3456);
    }

    @Test(priority = 1, groups = "pets")
    public void testAddPet() {
        String reqBody = "{\n" +
                "  \"id\": 3456,\n" +
                "  \"name\": \"doggie\",\n" +
                "  \"status\": \"available\"\n" +
                "}";

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(reqBody)
                .when()
                .post("/pet")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Pet added successfully");
        } else {
            logger.error("Error adding the pet, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 2, dependsOnMethods = "testAddPet", groups = "pets")
    public void testUpdatePet() {
        String reqBody = "{\n" +
                "  \"id\": 3456,\n" +
                "  \"name\": \"kitty\",\n" +
                "  \"status\": \"sold\"\n" +
                "}";

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(reqBody)
                .when()
                .put("/pet")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Pet updated successfully");
        } else {
            logger.error("Error updating the pet, service response: \n" + response.asString());
        }

        Gson g = new Gson();
        Pet pet = g.fromJson(response.asString(), Pet.class);

        assertEquals(response.getStatusCode(), 200);
        assertEquals(pet.getStatus(), "sold");
    }

    @Test(priority = 3, dependsOnMethods = "testUpdatePet", groups = "pets")
    public void testFindPet() {
        Response response = RestAssured.given()
                .when()
                .get("/pet/3456")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Pet received successfully");
        } else {
            logger.error("Error getting the pet, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }


    @Test(priority = 4, dependsOnMethods = "testFindPet", groups = "pets")
    public void testDeletePet() {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/pet/3456")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Pet deleted successfully");
        } else {
            logger.error("Error deleting the pet, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 5, dependsOnMethods = "testDeletePet", groups = "pets")
    public void testFindDeletedPet() {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/pet/3456")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 404) {
            logger.info("Operation successfully failed!");
        } else {
            logger.error("Deleted pet shouldn't be found: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 404);
    }
}
