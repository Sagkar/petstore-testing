package controllers;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StoreControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(PetControllerTest.class);

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2/store";
    }

    @AfterClass
    public void tearDown() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/order/1234")
                .then()
                .extract()
                .response();
    }

    @Test(priority = 1, groups = "store")
    public void testPlaceOrder() {
        String reqBody = "{\n" +
                "  \"id\": 1234,\n" +
                "  \"petId\": 0,\n" +
                "  \"quantity\": 0,\n" +
                "  \"shipDate\": \"2023-11-28T20:20:32.573Z\",\n" +
                "  \"status\": \"placed\",\n" +
                "  \"complete\": true\n" +
                "}";

        Response response = given()
                .contentType("application/json")
                .body(reqBody)
                .post("/order");

        if (response.getStatusCode() == 200) {
            logger.info("Order created successfully");
        } else {
            logger.error("Error creating order, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 2, dependsOnMethods = "testPlaceOrder", groups = "store")
    public void testFindPurchase() {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/order/1234")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Purchase received successfully");
        } else {
            logger.error("Error receiving a purchase, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 3, dependsOnMethods = "testFindPurchase", groups = "store")
    public void testGetInventory() {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/inventory")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Inventory received successfully");
        } else {
            logger.error("Error receiving a inventory, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(priority = 4, dependsOnMethods = "testGetInventory", groups = "store")
    public void testDeletePurchase() {
        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/order/1234")
                .then()
                .extract()
                .response();

        if (response.getStatusCode() == 200) {
            logger.info("Purchase deleted successfully");
        } else {
            logger.error("Error deleting a purchase, service response: \n" + response.asString());
        }

        assertEquals(response.getStatusCode(), 200);
    }
}
