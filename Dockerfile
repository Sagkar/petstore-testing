FROM openjdk:11

ENV PROJECT_DIR /usr/src/app

WORKDIR $PROJECT_DIR

COPY build.gradle settings.gradle $PROJECT_DIR/

COPY . .

RUN ./gradlew build

CMD ./gradlew test

EXPOSE 8099